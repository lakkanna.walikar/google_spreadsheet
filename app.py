from auth.googleauth import Auth
from handlers.writeintofiles import Writer

class App(Auth, Writer):
    # make fetched data simpler to use
    @staticmethod
    def data_normaliser(rawdata):
        if rawdata is not None:
            #rawdata = rawdata
            headers = []
            values = []
            for key,value in rawdata[0].items():
                headers.append(key)
                values.append(value)
            return headers, values
        else:
            return None,None

    # fetch data from google spreadsheet
    @classmethod
    def fetchdata(cls):
        client = Auth.authentication()
        if client is not None:
            try:
                # Find a workbook by name and open the first sheet
                # Make sure you use the right name here
                sheet = client.open("gspreadsheet").sheet1
                # Extract and print all of the values
                list_of_hashes = sheet.get_all_records()
                #print(list_of_hashes)
                return list_of_hashes
            except:
                print("Can't read the spreadsheet...!")

    def __enter__(self):
        #print("entered into with")
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_val is not None:
            print("Some error {0}".format(exc_val))

    @classmethod
    def initialise(cls):
        cls.data_dict = cls.fetchdata()
        if cls.data_dict is not None:
            cls.headers, cls.values = cls.data_normaliser(cls.data_dict)
            cls.write_csv = Writer.write_into_csv(cls.headers,cls.data_dict)
            cls.write_ini = Writer.write_into_ini(cls.headers, cls.values, cls.data_dict)

with App() as obj:
    obj.initialise()
