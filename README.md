# google_spreadsheet

### Steps to create google service account
* Go to [google developer console](https://console.developers.google.com/projectcreate?) and create project.
* In Dashboard eneble apy by clicking on **Enable Api and Services**
* Search for [Google drive api](https://console.developers.google.com/apis/library/drive.googleapis.com?) and enable it.
* [Create credentials] (https://console.developers.google.com/apis/credentials/wizard?)
    * Which API are you using? choose **_Google Drive Api_**
    * Where will you be calling the API from? choose **_Web server (eg. Node JS, Tomcat)_**
    * What data will you be accessing? select **_Application Data_**
    * Are you planning to use this API with App Engine or Compute Engine? choose **_No, I'm not using them_** 
    * Click on [What credentials do i need?] (https://console.developers.google.com/apis/credentials/wizard?)
    * Create service account if already does'nt exist
        * Service account name: _give new name_
        * Select role : In project --> _editor or owner. etc..,_
        * Key type : _JSON_
        * click continue
        * After this automatically json file credential will be downloaded. If not find get credentials button and download
    * After these steps paste the credentials json file in **_auth directory_**
* Create or open your google sheet **_Share_** here paste the client_email id which you get in credential json file
* submit

Note: Make sure that you shared your spread to the service account's json file client email id.