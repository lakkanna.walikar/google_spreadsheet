import csv
import configparser
import os
from constants import CSV_FILENAME, INI_FILENAME

class Writer(object):

    @staticmethod
    def get_path():
        cur_dir = os.path.abspath(os.path.curdir)
        dir_name = os.path.basename(cur_dir)
        if dir_name == 'handlers':
            file_path = r'../output/'
            return file_path
        else:
            file_path = r'output/'
            return file_path

    @staticmethod
    def write_into_ini(headers, values, data_dict):
        try:
            config = configparser.RawConfigParser()
        except:
            print("Failed to initialise config parser")
        # Please note that using RawConfigParser's set functions, you can assign
        # non-string values to keys internally, but will receive an error when
        # attempting to write to a file or when you get it in non-raw mode. Setting
        # values using the mapping protocol or ConfigParser's set() does not allow
        # such assignments to take place.
        index = 0

        try:

            for key, value in data_dict[0].items():
                section = 'Row'+str(index + 1)
                config.add_section(section)
                inner_index = -1 
                for v in values:
                
                    inner_index += 1
                    config.set(section, headers[inner_index], values[inner_index])
                    #config.set('Section1', 'foo', '%(bar)s is %(baz)s!')
                index += 1
                # Writing our configuration file to 'example.cfg'
            file_path = Writer.get_path()
            with open(file_path + INI_FILENAME, 'w') as configfile:

                print("Writing into {0} file.....".format(INI_FILENAME))
                try:
                    config.write(configfile)
                    print("Completed writing into ini file...!")
                except:
                    print("Error while writing into ini file...!")
            #print(filename, headers, values)
        except:
                print("Error while writing into {0} file....!".format(INI_FILENAME))

    # write data into csv file
    # headers contrains the headers of the data
    # data_dict is dictionary contains headers and values
    @staticmethod 
    def write_into_csv(headers, data_dict):
        try:
            file_path = Writer.get_path()
            with open(file_path + CSV_FILENAME, 'w') as csvfile:
                print("Writing into {0} file....".format(CSV_FILENAME))
                writer = csv.DictWriter(csvfile, fieldnames=headers)
                writer.writeheader()
                writer.writerows(data_dict)
                print("Completed writing into csv file...!")
        except:
            print("Error occured while writint into csv file")

    @classmethod
    def initialise(cls):
        print("INITIALISE")
        cls.headers = ['header1', 'header2']
        cls.values = ['value1', 'value2']
        cls.data_dict = [{'header1':'value1', 'header2':'value2'}]
        cls.write_into_csv(cls.headers, cls.data_dict)
        cls.write_into_ini(cls.headers, cls.values, cls.data_dict)

if __name__ == '__main__':
    Writer.initialise()
