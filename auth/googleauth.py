#!/usr/bin/python
__author__ = 'lakkanna walikar'
import gspread
from oauth2client.service_account import ServiceAccountCredentials

class Auth(object):

    @classmethod 
    def authentication(cls):
        print("Authenticating...")
        # use creds to create a client to interact with the Google Drive API
        scope = ['https://spreadsheets.google.com/feeds',
                                 'https://www.googleapis.com/auth/drive']
        creds = ServiceAccountCredentials.from_json_keyfile_name("auth/credentials.json", scope)
        try:
            client = gspread.authorize(creds)
            if client is not None:
                print("Authenticated successfully..!")
                return client
            else:
                return None

        except:
            print("Failed to authenticate, make sure you have sufficient internet")


if __name__ =='__main__':
    Auth.authentication()
